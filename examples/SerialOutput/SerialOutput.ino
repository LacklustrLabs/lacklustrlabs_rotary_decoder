#include <lacklustrlabs_rotary_decoder.h>
#include <limits.h>

int32_t state; 
constexpr uint8_t QUAD_A_PIN = 2;
constexpr uint8_t QUAD_B_PIN = 3;

using namespace lacklustrlabs;

#ifndef SOUT
#define SOUT Serial
#endif

RotaryDecoder decoder(QUAD_A_PIN, QUAD_B_PIN);

void setup() {
  SOUT.begin(38400);
  decoder.begin();
  state = -LONG_MAX;
  SOUT.println(F("Starting Quad Decoder.."));
}

void loop() { 
  // >>2 is for a quadrature encoder
  int32_t newState = decoder.getState()>>2;
  if (state != newState) {
    SOUT.print(F("Quad decoder is now at "));
    SOUT.println(newState);
    state = newState;
  }
}