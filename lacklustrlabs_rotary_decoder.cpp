#include "Arduino.h"
#include "lacklustrlabs_rotary_decoder.h"

using namespace lacklustrlabs;

#ifndef LACKLUSTRLABS_MENU_DEBUG
#define LACKLUSTRLABS_MENU_DEBUG(x)
#endif

#ifdef LACKLUSTRLABS_DECODER_INTERRUPT_NO_ARGUMENTS
static RotaryDecoder *instance = nullptr;
#endif

void RotaryDecoder::begin(){

    pinMode(_pinA, INPUT); // don't use internal pullup or pulldown because the built in resistors are
    pinMode(_pinB, INPUT); // just too weak to reliably affect external components.

#if defined(ARDUINO_ARCH_STM32F1)
#if defined(LACKLUSTRLABS_DEBUG)
    if(LACKLUSTRLABS_DEBUG_PORT && lacklustrlabs::getGpioRegMap(_pinA) != lacklustrlabs::getGpioRegMap(_pinB)) {
      LACKLUSTRLABS_MENU_DEBUG("Error: Rotary(): pinA and pinB must be on the same port");
    }
    if (LACKLUSTRLABS_DEBUG_PORT && _pinA_bit >= _pinB_bit) {
      LACKLUSTRLABS_MENU_DEBUG("Error: Rotary(): pinbit A must be lower than pinbit B");
    }
#endif // LACKLUSTRLABS_DEBUG
#endif

#if !defined(LACKLUSTRLABS_DECODER_INTERRUPT_NO_ARGUMENTS)
    switch (_onEdge) {
    case CHANGE_:
      attachInterrupt(_pinA, interruptMethod_change, (void*) this, CHANGE);
      attachInterrupt(_pinB, interruptMethod_change, (void*) this, CHANGE);
      break;
    case FALLING_:
      attachInterrupt(_pinA, interruptMethod_falling, (void*) this, FALLING);
      attachInterrupt(_pinB, interruptMethod_falling, (void*) this, FALLING);
      break;
    default:
      attachInterrupt(_pinA, interruptMethod_rising, (void*) this, RISING);
      attachInterrupt(_pinB, interruptMethod_rising, (void*) this, RISING);
    }

#elif defined(LACKLUSTRLABS_DECODER_INTERRUPT_NO_ARGUMENTS)
    instance = this;
    switch (_onEdge) {
    case CHANGE_:
      attachInterrupt(digitalPinToInterrupt(_pinA), interruptMethod_change, CHANGE);
      attachInterrupt(digitalPinToInterrupt(_pinB), interruptMethod_change, CHANGE);
      break;
    case FALLING_:
      attachInterrupt(digitalPinToInterrupt(_pinA), interruptMethod_falling, FALLING);
      attachInterrupt(digitalPinToInterrupt(_pinB), interruptMethod_falling, FALLING);
      break;
    default:
      attachInterrupt(digitalPinToInterrupt(_pinA), interruptMethod_rising, RISING);
      attachInterrupt(digitalPinToInterrupt(_pinB), interruptMethod_rising, RISING);
    }
#endif
}

void lacklustrlabs::interruptMethod_change(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS) {
  LACKLUSTRLABS_DECODER_INTERRUPT_SINGLETON
                                        //0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15
  static const int8_t encoderStates[]  = {0,-1, 1, 0, 1, 0, 0,-1,-1, 0, 0, 1, 0, 1,-1, 0}; // for CHANGE_
  uint8_t state = ((instance->_prevState << 2) | (instance->getDecoderInput())) & 0b00001111;
  instance->_encoderTicks += encoderStates[state];
  instance->_prevState = state;
}

void lacklustrlabs::interruptMethod_rising(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS) {
  LACKLUSTRLABS_DECODER_INTERRUPT_SINGLETON
                                        //0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15
  static const int8_t encoderStates[]  = {0,-1, 1, 0, 0, 0, 0,-1, 0, 0, 0, 1, 0, 0, 0, 0}; // for RISING_

  uint8_t state = ((instance->_prevState << 2) | (instance->getDecoderInput())) & 0b00001111;
  instance->_encoderTicks += encoderStates[state];
  instance->_prevState = state;
}

void lacklustrlabs::interruptMethod_falling(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS) {
  LACKLUSTRLABS_DECODER_INTERRUPT_SINGLETON
                                          //0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15
  static const int8_t encoderStates[]    = {0, 0, 0, 0, 1, 0, 0, 0,-1, 0, 0, 0, 0, 1,-1, 0}; // for FALLING_

  uint8_t state = ((instance->_prevState << 2) | (instance->getDecoderInput())) & 0b00001111;
  instance->_encoderTicks += encoderStates[state];
  instance->_prevState = state;
}

