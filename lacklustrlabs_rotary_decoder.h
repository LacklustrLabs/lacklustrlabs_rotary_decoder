#pragma once

#if defined(ARDUINO) && ARDUINO >=100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif
#include <assert.h>

namespace lacklustrlabs {

enum OnEdge {CHANGE_=CHANGE, FALLING_=FALLING, RISING_=RISING};

#if defined(ARDUINO_AVR_MEGA2560) || defined(__AVR_ATmega328P__) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_PRO)
#define LACKLUSTRLABS_DECODER_INTERRUPT_NO_ARGUMENTS
#define LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS
#define LACKLUSTRLABS_DECODER_INTERRUPT_SINGLETON
#elif defined(ARDUINO_ARCH_STM32F1)
#define LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS void * voidp
#define LACKLUSTRLABS_DECODER_INTERRUPT_SINGLETON RotaryDecoder* instance = (RotaryDecoder*)voidp;

#endif

class RotaryDecoder;

void interruptMethod_change(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);
void interruptMethod_rising(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);
void interruptMethod_falling(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);

class RotaryDecoder {
  friend void interruptMethod_change(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);
  friend void interruptMethod_rising(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);
  friend void interruptMethod_falling(LACKLUSTRLABS_DECODER_INTERRUPT_ARGUMENTS);

public:
#if defined(ARDUINO_ARCH_STM32F1)
  const volatile uint32_t* _gpioInput;
  const uint8_t _pinA_bit;
  const uint8_t _pinB_bit;

  RotaryDecoder(uint8_t pinA, uint8_t pinB, OnEdge onEdge=CHANGE_) :
    _gpioInput(portInputRegister(digitalPinToPort(pinA))),
    _pinA_bit(PIN_MAP[pinA].gpio_bit),
    _pinB_bit(PIN_MAP[pinB].gpio_bit),
    _pinA(pinA),
    _pinB(pinB),
    _onEdge(onEdge),
    _encoderTicks(0),
    _prevState(0) {
  }

#elif defined(ARDUINO_AVR_MEGA2560) || defined(__AVR_ATmega328P__) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_PRO)
  RotaryDecoder(uint8_t pinA=0, uint8_t pinB=0, OnEdge onEdge=CHANGE_) :
    _pinA(pinA),
    _pinB(pinB),
    _onEdge(onEdge),
    _encoderTicks(0),
    _prevState(0){
    assert((pinA!=pinB)&&(pinA==2||pinA==3)&&(pinB==2||pinB==3));//,"only pin 2 and 3 can be used (for now)");
  }
#else
#error "Unsupported board"
#endif

  void begin();

  RotaryDecoder( const RotaryDecoder& other ) = delete; // non construction-copyable
  RotaryDecoder& operator=( const RotaryDecoder& ) = delete; // non copyable

  inline uint8_t getDecoderInput() {
#if defined(ARDUINO_ARCH_STM32F1)
    uint16_t sample = *_gpioInput; // only read the low 16 bits
    return ((sample>>_pinA_bit)&0b01)|(((sample>>(_pinB_bit))&0b01))<<1;
#elif defined(ARDUINO_AVR_MEGA2560)
    return ((PINE&0b00110000)>>4)&0b00000011; // always pin 2 & 3
#elif defined(__AVR_ATmega328P__) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_PRO)
    return ((PIND&0b00001100)>>2)&0b00000011; // always pin 2 & 3
#endif
  }

  int32_t getState() {
    return _encoderTicks;
  }

protected:
  uint8_t _pinA;
  uint8_t _pinB;
  OnEdge _onEdge;

  volatile int32_t _encoderTicks;
  volatile uint8_t _prevState;
};

} // lacklustrlabs

